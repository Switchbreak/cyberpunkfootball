/**
 * Simplest enemy type
 * @author Switchbreak
 */

const snakeDazeTime = 1500;
const snakeBlinkTime = 30;
const snakeTargetRangeSq = 100 * 100;
const snakeChargeSpeed = 300;
const snakeChargeTime = 100;

Snake = function(game, startX, startY, targets) {
	this.game = game;
	this.dazed = 0;
	this.blinkTimer = 0;
	this.targets = targets;
	this.targetPoint = null;
	this.chargeTime = 0;
	
	this.shadow = this.game.add.sprite( 0, 0, 'shadow' );
	
	this.sprite = game.add.sprite( 0, 0, 'basher' );
	this.sprite.centerOn( startX, startY );
	
	this.sprite.animations.add('idle', [0, 1, 2, 3], 10, true);
	this.sprite.animations.add('walk', [4, 5, 6, 7], 10, true);
	this.sprite.animations.play('idle');
	
	this.sprite.body.bounce.x = 0.5;
	this.sprite.body.bounce.y = 0.5;
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.setSize( 40, 40, 0, 0 );
	this.sprite.body.immovable = true;
	
	this.sprite.body.velocity.x = snakeChargeSpeed;
	this.sprite.body.velocity.y = snakeChargeSpeed;
	
	this.sprite.agent = this;
};

Snake.prototype.destroy = function() {
	this.sprite.destroy();
	this.shadow.destroy();	
};

Snake.prototype.daze = function() {
	this.dazed = snakeDazeTime;
	this.targetPoint = null;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.sprite.animations.stop();
	
	var knockback = this.game.add.tween( this.sprite );
	knockback.to( {y: this.sprite.y - 30 }, 500, Phaser.Easing.Bounce.Out );
	knockback.start();
};

Snake.prototype.update = function() {
	if( this.dazed > 0 ) {
		this.dazed -= this.game.time.elapsed;
		this.blink();
	}
	else {
		this.AI();
	}
	
	this.shadow.x = this.sprite.x;
	this.shadow.y = this.sprite.y + 30;
};

Snake.prototype.blink = function() {
	if( this.dazed <= 0 ) {
		this.sprite.visible = true;
		this.sprite.exists = true;
		
		this.sprite.body.velocity.x = snakeChargeSpeed;
		this.sprite.body.velocity.y = snakeChargeSpeed;
	}
	else {
		if( this.blinkTimer <= 0 ) {
			this.sprite.visible = !this.sprite.visible;
			this.blinkTimer = snakeBlinkTime;
		}
		this.blinkTimer -= this.game.time.elapsed;
	}
};

Snake.prototype.AI = function() {
	this.sprite.animations.play('walk');
	
	if( this.sprite.y > 600 )
		this.sprite.body.velocity.y = -snakeChargeSpeed;
};
