/**
 * Simplest enemy type
 * @author Switchbreak
 */

const bruiserDazeTime = 1500;
const bruiserBlinkTime = 30;
const bruiserTargetRangeSq = 100 * 100;
const bruiserChargeSpeed = 1000;
const bruiserChargeTime = 100;

Bruiser = function(game, startX, startY, targets) {
	this.game = game;
	this.dazed = 0;
	this.blinkTimer = 0;
	this.targets = targets;
	this.targetPoint = null;
	this.chargeTime = 0;
	
	this.shadow = this.game.add.sprite( 0, 0, 'shadow' );
	
	this.sprite = game.add.sprite( 0, 0, 'basher' );
	this.sprite.centerOn( startX, startY );
	
	this.sprite.animations.add('idle', [0, 1, 2, 3], 10, true);
	this.sprite.animations.add('walk', [4, 5, 6, 7], 10, true);
	this.sprite.animations.play('idle');
	
	this.sprite.body.bounce.x = 0.5;
	this.sprite.body.bounce.y = 0.5;
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.setSize( 40, 40, 0, 0 );
	this.sprite.body.immovable = true;
	
	this.sprite.agent = this;
};

Bruiser.prototype.resetBruiser = function( startX, startY ) {
	this.sprite.x = startX;
	this.sprite.y = startY;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.dazed = 0;
	this.targetPoint = null;
};

Bruiser.prototype.destroy = function() {
	this.sprite.destroy();
	this.shadow.destroy();	
};

Bruiser.prototype.daze = function() {
	this.dazed = bruiserDazeTime;
	this.targetPoint = null;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.sprite.animations.stop();
	
	var knockback = this.game.add.tween( this.sprite );
	knockback.to( {y: this.sprite.y - 30 }, 500, Phaser.Easing.Bounce.Out );
	knockback.start();
};

Bruiser.prototype.update = function() {
	if( this.dazed > 0 ) {
		this.dazed -= this.game.time.elapsed;
		this.blink();
	}
	else {
		this.AI();
	}
	
	this.shadow.x = this.sprite.x;
	this.shadow.y = this.sprite.y + 30;
};

Bruiser.prototype.blink = function() {
	if( this.dazed <= 0 ) {
		this.sprite.visible = true;
		this.sprite.exists = true;
	}
	else {
		if( this.blinkTimer <= 0 ) {
			this.sprite.visible = !this.sprite.visible;
			this.blinkTimer = bruiserBlinkTime;
		}
		this.blinkTimer -= this.game.time.elapsed;
	}
};

Bruiser.prototype.AI = function() {
	if( this.targetPoint == null ) {
		var minimumDistanceSq = bruiserTargetRangeSq;
		for( var i = 0; i < this.targets.length; i++ ) {
			if( this.targets[i].dazed <= 0 ) {
				var targetVector = Phaser.Point.subtract( this.sprite.center, this.targets[i].sprite.center );
				var distanceSq = targetVector.x * targetVector.x + targetVector.y * targetVector.y;
				
				if( distanceSq < minimumDistanceSq ) {
					this.targetPoint = this.targets[i].sprite.center.clone();
					minimumDistanceSq = distanceSq;
					this.chargeTime = bruiserChargeTime;
				}
			}
		}
		
		if( this.targetPoint == null ) {
			this.sprite.body.velocity.y = 10;
			this.sprite.animations.play('walk');
		}
	}
	else {
		this.game.physics.moveToObject( this.sprite, this.targetPoint, chargeSpeed );
		
		if( this.chargeTime > 0 )
			this.chargeTime -= this.game.time.elapsed;
		else {
			this.targetPoint = null;
			this.sprite.body.velocity.x = 0;
			this.sprite.body.velocity.y = 0;
		}
	}
};
