/**
 * Simplest enemy type
 * @author Switchbreak
 */

const stochasticDazeTime = 1500;
const stochasticBlinkTime = 30;
const stochasticChargeSpeed = 200;

Stochastic = function(game, startX, startY, targets) {
	this.game = game;
	this.dazed = 0;
	this.blinkTimer = 0;
	this.targets = targets;
	
	this.shadow = this.game.add.sprite( 0, 0, 'shadow' );
	
	this.sprite = game.add.sprite( 0, 0, 'stochastic' );
	this.sprite.centerOn( startX, startY );
	
	this.sprite.animations.add('idle', [0, 1, 2, 3], 10, true);
	this.sprite.animations.add('walk', [4, 5, 6, 7], 10, true);
	this.sprite.animations.play('walk');
	
	this.sprite.body.bounce.x = 0.5;
	this.sprite.body.bounce.y = 0.5;
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.setSize( 40, 40, 0, 0 );
	this.sprite.body.immovable = true;
	
	this.sprite.agent = this;
};

Stochastic.prototype.destroy = function() {
	this.sprite.destroy();
	this.shadow.destroy();	
};

Stochastic.prototype.daze = function() {
	this.dazed = stochasticDazeTime;
	this.targetPoint = null;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.sprite.animations.stop();
	
	var knockback = this.game.add.tween( this.sprite );
	knockback.to( {y: this.sprite.y - 30 }, 500, Phaser.Easing.Bounce.Out );
	knockback.start();
};

Stochastic.prototype.update = function() {
	if( this.dazed > 0 ) {
		this.dazed -= this.game.time.elapsed;
		this.blink();
	}
	else {
		this.AI();
	}
	
	this.shadow.x = this.sprite.x;
	this.shadow.y = this.sprite.y + 30;
};

Stochastic.prototype.blink = function() {
	if( this.dazed <= 0 ) {
		this.sprite.visible = true;
		this.sprite.exists = true;
	}
	else {
		if( this.blinkTimer <= 0 ) {
			this.sprite.visible = !this.sprite.visible;
			this.blinkTimer = stochasticBlinkTime;
		}
		this.blinkTimer -= this.game.time.elapsed;
	}
};

Stochastic.prototype.AI = function() {
	this.powerupTime -= this.game.time.elapsed;
	
	var angle = this.game.rnd.angle();
	this.sprite.body.velocity.x = Math.cos(angle) * stochasticChargeSpeed;
	this.sprite.body.velocity.y = Math.sin(angle) * stochasticChargeSpeed;
};
