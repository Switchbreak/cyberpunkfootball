/**
 * Simplest enemy type
 * @author Switchbreak
 */

const chargerDazeTime = 1500;
const chargerBlinkTime = 30;
const chargerChargeSpeed = 1000;
const chargerChargeTime = 500;
const chargerPowerUpTime = 2000;

Charger = function(game, startX, startY, targets) {
	this.game = game;
	this.dazed = 0;
	this.blinkTimer = 0;
	this.targets = targets;
	this.chargeTime = 0;
	this.powerupTime = chargerPowerUpTime;
	
	this.shadow = this.game.add.sprite( 0, 0, 'shadow' );
	
	this.sprite = game.add.sprite( 0, 0, 'charger' );
	this.sprite.centerOn( startX, startY );
	
	this.sprite.animations.add('idle', [0, 1, 2, 3], 10, true);
	this.sprite.animations.add('walk', [4, 5, 6, 7], 10, true);
	this.sprite.animations.play('idle');
	
	this.sprite.body.bounce.x = 0.5;
	this.sprite.body.bounce.y = 0.5;
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.setSize( 40, 40, 0, 0 );
	this.sprite.body.immovable = true;
	
	this.sprite.agent = this;
};

Charger.prototype.destroy = function() {
	this.sprite.destroy();
	this.shadow.destroy();	
};

Charger.prototype.daze = function() {
	this.dazed = chargerDazeTime;
	this.targetPoint = null;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.sprite.animations.stop();
	
	var knockback = this.game.add.tween( this.sprite );
	knockback.to( {y: this.sprite.y - 30 }, 500, Phaser.Easing.Bounce.Out );
	knockback.start();
};

Charger.prototype.update = function() {
	if( this.dazed > 0 ) {
		this.dazed -= this.game.time.elapsed;
		this.blink();
	}
	else {
		this.AI();
	}
	
	this.shadow.x = this.sprite.x;
	this.shadow.y = this.sprite.y + 30;
};

Charger.prototype.blink = function() {
	if( this.dazed <= 0 ) {
		this.sprite.visible = true;
		this.sprite.exists = true;
	}
	else {
		if( this.blinkTimer <= 0 ) {
			this.sprite.visible = !this.sprite.visible;
			this.blinkTimer = chargerBlinkTime;
		}
		this.blinkTimer -= this.game.time.elapsed;
	}
};

Charger.prototype.AI = function() {
	if( this.chargeTime <= 0 ) {
		this.sprite.animations.play('idle');
		this.powerupTime -= this.game.time.elapsed;
		
		if( this.powerupTime <= 0 ) {
			var target = this.targets[this.game.rnd.integerInRange(0, this.targets.length)];
			
			var angle = this.game.math.angleBetween( this.sprite.center.x, this.sprite.center.y, target.sprite.center.x, target.sprite.center.y );
			this.sprite.body.velocity.x = Math.cos(angle) * chargerChargeSpeed;
			this.sprite.body.velocity.y = Math.sin(angle) * chargerChargeSpeed;
			
			this.chargeTime = chargerChargeTime;
		}
	}
	else {
		this.sprite.animations.play('walk');
		this.chargeTime -= this.game.time.elapsed;
		
		if( this.chargeTime <= 0 ) {
			this.powerupTime = chargerPowerUpTime;
			this.sprite.body.velocity.x = 0;
			this.sprite.body.velocity.y = 0;
		}
	}
};
