/**
 * Hacker Agent is either the player or another agent on the player's team
 * @author Switchbreak
 */

const movementSpeed = 300;
const numberStyle = { font: "20px Arial", fill: "#000000", align: "center", width: "40px" };
const dazeTime = 1000;
const blinkTime = 30;
const targetRangeSq = 100 * 100;
const chargeSpeed = 1000;
const chargeTime = 100;

HackerAgent = function (game, startX, startY, playerNumber, isAgent, targets) {
	this.game = game;
	this.playerNumber = playerNumber;
	this.tracingRoute = false;
	this.runningRoute = false;
	this.dazed = 0;
	this.blinkTimer = 0;
	this.route = [];
	this.targets = targets;
	this.targetPoint = null;
	this.chargeTime = 0;
	
	this.createGraphic( startX, startY, playerNumber, isAgent );
	this.setAgent( isAgent );
};

HackerAgent.prototype.createGraphic = function( startX, startY ) {
	this.routeGraphic = this.game.add.graphics( 0, 0 );
	
	this.shadow = this.game.add.sprite( 0, 0, 'shadow' );
	
	this.sprite = this.game.add.sprite( 0, 0, 'agent' );
	this.sprite.centerOn( startX, startY );
	
	this.sprite.animations.add('idle', [0, 1, 2, 3], 10, true);
	this.sprite.animations.add('idle2', [8, 9, 10, 11], 10, true);
	this.sprite.animations.add('walk', [4, 5, 6, 7], 10, true);
	this.sprite.animations.add('walk2', [12, 13, 14, 15], 10, true);
	
	this.sprite.body.bounce.x = 0.5;
	this.sprite.body.bounce.y = 0.5;
	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.setSize( 40, 40, 0, 0 );
	
	this.sprite.agent = this;
	
	this.number = this.game.add.text( startX - 12, startY - 22, this.playerNumber, numberStyle );
};

HackerAgent.prototype.resetAgent = function( startX, startY, isAgent ) {
	this.sprite.centerOn( startX, startY );
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	this.tracingRoute = false;
	this.runningRoute = false;
	this.dazed = 0;
	this.targetPoint = null;
	
	this.route.length = 0;
	this.drawRoute();
	
	this.setAgent( isAgent );
};

HackerAgent.prototype.daze = function() {
	this.dazed = dazeTime;
	this.targetPoint = null;
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	this.sprite.animations.stop();
	
	var knockback = this.game.add.tween( this.sprite );
	knockback.to( {y: this.sprite.y + 30 }, 500, Phaser.Easing.Bounce.Out );
	knockback.start();
	
	if( this.isAgent ) {
		// TODO: LOSE THE GAME
	}
	else {
		this.route.length = 0;
		this.drawRoute();
	}
};

HackerAgent.prototype.update = function() {
	if( this.isAgent ) {
		this.agentUpdate();
	}
	else {
		this.blockerUpdate();
	}
	
	if( this.dazed > 0 ) {
		this.dazed -= this.game.time.elapsed;
		this.blink();
	}
	
	this.shadow.x = this.sprite.x;
	this.shadow.y = this.sprite.y + 30;
};

HackerAgent.prototype.blink = function() {
	if( this.dazed <= 0 ) {
		this.sprite.visible = true;
	}
	else {
		if( this.blinkTimer <= 0 ) {
			this.sprite.visible = !this.sprite.visible;
			this.blinkTimer = blinkTime;
		}
		this.blinkTimer -= this.game.time.elapsed;
	}
};

HackerAgent.prototype.agentUpdate = function() {
	this.sprite.body.velocity.x = 0;
	this.sprite.body.velocity.y = 0;
	
	if( this.dazed <= 0 ) {
		if( this.game.input.keyboard.isDown( Phaser.Keyboard.LEFT ) || this.game.input.keyboard.isDown( Phaser.Keyboard.A ) )
			this.sprite.body.velocity.x = -movementSpeed;
		if( this.game.input.keyboard.isDown( Phaser.Keyboard.RIGHT ) || this.game.input.keyboard.isDown( Phaser.Keyboard.D ) )
			this.sprite.body.velocity.x = movementSpeed;
		if( this.game.input.keyboard.isDown( Phaser.Keyboard.UP ) || this.game.input.keyboard.isDown( Phaser.Keyboard.W ) )
			this.sprite.body.velocity.y = -movementSpeed;
		if( this.game.input.keyboard.isDown( Phaser.Keyboard.DOWN ) || this.game.input.keyboard.isDown( Phaser.Keyboard.S ) )
			this.sprite.body.velocity.y = movementSpeed;
		
		if( this.sprite.body.velocity.x != 0 || this.sprite.body.velocity.y != 0 )
			this.sprite.animations.play( 'walk' );
		else
			this.sprite.animations.play( 'idle' );
	}
};

HackerAgent.prototype.blockerUpdate = function() {
	if( this.runningRoute ) {
		this.moveAlongRoute();
		this.drawRoute();
	}
	else {
		this.sprite.body.velocity.x = 0;
		this.sprite.body.velocity.y = 0;
		
		if( this.game.input.keyboard.isDown( Phaser.Keyboard.X ) ) {
			this.runningRoute = true;
			this.tracingRoute = false;
			this.sprite.animations.play( 'walk2' );
		}
		else if( this.tracingRoute ) {
			if( this.game.input.mousePointer.isDown ) {
				if( this.route.length == 0 || !this.game.input.mousePointer.position.equals( this.route[this.route.length - 1] ) ) {
					this.route.push( this.game.input.mousePointer.position.clone() );
					this.drawRoute();
				}
			}
			else {
				this.tracingRoute = false;
			}
		}
	}
		
	if( !this.tracingRoute && this.game.input.mousePointer.justPressed() ) {
		if( this.game.input.mousePointer.x > this.sprite.x && this.game.input.mousePointer.x < this.sprite.body.right
				&& this.game.input.mousePointer.y > this.sprite.y && this.game.input.mousePointer.y < this.sprite.body.bottom ) {
			this.tracingRoute = true;
			this.runningRoute = false;
			this.route.length = 0;
			this.drawRoute();
		}
	}
	
	if( this.dazed <= 0 )
		this.AI();
	this.positionNumber();
};

HackerAgent.prototype.moveAlongRoute = function() {
	var distance = this.game.time.physicsElapsed * movementSpeed * 2;
	var position = this.sprite.center;
	var routeDist = 0;
	
	while( routeDist < distance && this.route.length > 0 ) {
		var nodeDist = position.distance( this.route[0] );
		routeDist += nodeDist;
		
		if( routeDist >= distance ) {
			var diff = Phaser.Point.subtract( this.route[0], position );
			var scale = (nodeDist - (routeDist - distance)) / nodeDist;
			position.add( diff.x * scale, diff.y * scale );
		}
		else {
			position = this.route.shift();
		}
	}
	
	if( this.route.length == 0 )
		this.runningRoute = false;
	
	this.sprite.body.velocity.x = (position.x - 20 - this.sprite.body.x) / this.game.time.physicsElapsed;
	this.sprite.body.velocity.y = (position.y - 20 - this.sprite.body.y) / this.game.time.physicsElapsed;
};

HackerAgent.prototype.AI = function() {
	if( this.targetPoint == null ) {
		if( !this.runningRoute )
			this.sprite.animations.play( 'idle2' );
		
		var minimumDistanceSq = targetRangeSq;
		for( var i = 0; i < this.targets.length; i++ ) {
			if( this.targets[i].dazed <= 0 ) {
				var targetVector = Phaser.Point.subtract( this.sprite.center, this.targets[i].sprite.center );
				var distanceSq = targetVector.x * targetVector.x + targetVector.y * targetVector.y;
				
				if( distanceSq < minimumDistanceSq ) {
					this.targetPoint = this.targets[i].sprite.center.clone();
					minimumDistanceSq = distanceSq;
					this.chargeTime = chargeTime;
					
					this.runningRoute = false;
					this.tracingRoute = false;
					this.route.length = 0;
					this.drawRoute();
					
					this.sprite.animations.play( 'walk2' );
				}
			}
		}
	}
	else {
		this.game.physics.moveToObject( this.sprite, this.targetPoint, chargeSpeed );
		
		if( this.chargeTime > 0 )
			this.chargeTime -= this.game.time.elapsed;
		else {
			this.targetPoint = null;
			this.sprite.body.velocity.x = 0;
			this.sprite.body.velocity.y = 0;
		}
	}
};

HackerAgent.prototype.drawRoute = function() {
	this.routeGraphic.clear();
	
	if( this.route.length <= 1 )
		return;		
	
	this.routeGraphic.lineStyle(5, 0xBF9430, 1.0);
	this.routeGraphic.moveTo( this.route[0].x, this.route[0].y );
	for( var i = 1; i < this.route.length; i++ ) {
		this.routeGraphic.lineTo( this.route[i].x, this.route[i].y );
	}
};

HackerAgent.prototype.setAgent = function( isAgent ) {
	this.isAgent = isAgent;
	
	if( isAgent ) {
		this.sprite.animations.play( 'idle' );
		this.sprite.body.immovable = false;
		this.number.visible = false;
		
		this.route.length = 0;
		this.drawRoute();
	}
	else {
		this.sprite.animations.play( 'idle2' );
		this.sprite.body.velocity.x = 0;
		this.sprite.body.velocity.y = 0;
		this.sprite.body.immovable = true;
		
		this.positionNumber();
		this.number.visible = true;
	}
};

HackerAgent.prototype.positionNumber = function() {
	this.number.x = this.sprite.x + 15;
	this.number.y = this.sprite.y - 25;
};
